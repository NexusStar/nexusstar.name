var idx = lunr(function () {
  this.field('title', {boost: 10})
  this.field('excerpt')
  this.field('categories')
  this.field('tags')
  this.ref('id')
});



  
  
    idx.add({
      title: "Use Drush to Import/Export MySQL database file",
      excerpt: "Export Database to file Drupal 6 &amp; 7 drush cc drush sql-dump &gt; ~/my-sql-dump-file-name.sql Drupal 8 drush cr drush sql-dump...",
      categories: [],
      tags: ["drush","Drupal","beginner"],
      id: 0
    });
    
  
    idx.add({
      title: "Favor object composition over class inheritance",
      excerpt: "“Design Patterns: Elements of Reusable Object-Oriented Software” Composition over inheritance (or composite reuse principle) in object-oriented programming (OOP) is the...",
      categories: [],
      tags: ["javascript","concepts"],
      id: 1
    });
    
  
    idx.add({
      title: "File line ending in Windows of git repositories without normalization",
      excerpt: "The proper way to get LF endings in Windows is to first set core.autocrlf to false: git config --global core.autocrlf...",
      categories: [],
      tags: ["git","advanced"],
      id: 2
    });
    
  
    idx.add({
      title: "Git made submodule from subfolder",
      excerpt: "At the beginning, we have something like this: &lt;git repository A&gt; someFolders someFiles someLib &lt;-- we want this to be...",
      categories: [],
      tags: ["git","beginner"],
      id: 3
    });
    
  
    idx.add({
      title: "Install / Update Ruby on windows",
      excerpt: "Install using downloads from RubyInstaller Check if correct version is installed with ruby --version if not correct version reported check...",
      categories: [],
      tags: ["begginer","ruby","windows"],
      id: 4
    });
    
  
    idx.add({
      title: "Install AUR package-query and yaourt",
      excerpt: "You can use the ~/temp/AUR/ to manually install AUR packages: $ mkdir -p ~/temp/AUR/ &amp;&amp; cd ~/temp/AUR/ First, we install...",
      categories: [],
      tags: ["beginner","ArchLinux"],
      id: 5
    });
    
  
    idx.add({
      title: "JavaScript as functional programming language",
      excerpt: "JavaScript has the most important features needed for functional proragmming: First class functions: The ability to use functions as data...",
      categories: [],
      tags: ["programming","beginner","JavaScript"],
      id: 6
    });
    
  
    idx.add({
      title: "Software composition notes",
      excerpt: "In mathematics and computer science curring is the technique of translating the evaluation of a function that takes multiple arguments...",
      categories: [],
      tags: ["programming","beginner"],
      id: 7
    });
    
  
    idx.add({
      title: "Responsive design",
      excerpt: "Frontend Masters course I am currently watching from Jen Kramer bring to memory the article from Ethan Marcote definning what...",
      categories: [],
      tags: ["web","beginner"],
      id: 8
    });
    
  
    idx.add({
      title: "Using Iedit mode in Spacemacs",
      excerpt: "Spacemacs defines two states iedit state iedit-insert state Iedit can be started from search with SPC s e Key Binding...",
      categories: [],
      tags: ["spacemacs","begginer"],
      id: 9
    });
    
  
    idx.add({
      title: "Spacemacs Cheat Sheet",
      excerpt: "Get help, quit and configure Editor Basic commands Key Binding Description SPC q q Quit SPC z x Increase/decrease font...",
      categories: [],
      tags: ["spacemacs","begginer"],
      id: 10
    });
    
  
    idx.add({
      title: "Shell script's' idiom 2>&1 recap",
      excerpt: "There is excelent article from Brian Storti back from 2015 about shell script’s idiom 2&gt;&amp;1 So to note important points...",
      categories: [],
      tags: ["linux","beginner"],
      id: 11
    });
    
  
    idx.add({
      title: "SVN changelist command",
      excerpt: "I’m not a big fan of SVN but it was inescapable evil in some projects. One of the best command...",
      categories: [],
      tags: ["svn","beginner"],
      id: 12
    });
    
  
    idx.add({
      title: "Replace ^M windows line endings in vim",
      excerpt: "When files comes from windows sometimes there are ^M at end of all lines which marks line ending. So to...",
      categories: [],
      tags: ["vim","beginner"],
      id: 13
    });
    
  
    idx.add({
      title: "Work with numbers in Vim",
      excerpt: "The expression register = &lt;C-a&gt; &lt;C-x&gt;in normal mode Expression register = The expression register can evaluate a piece of Vim...",
      categories: [],
      tags: ["vim","beginner"],
      id: 14
    });
    
  

  
  
    idx.add({
      title: "False confidence due to new found pride",
      excerpt: "Why do so many beginner programmers think they should build everything from scratch? Just read answers to this question on...",
      categories: [],
      tags: ["personal","programming"],
      id: 15
    });
    
  
    idx.add({
      title: "Rebuild with Jekyll",
      excerpt: "Why My personal website went to too many changes. I start with simple static html and JS and later went...",
      categories: [],
      tags: ["jekyll","personal","blog"],
      id: 16
    });
    
  
    idx.add({
      title: "C++ Lambda",
      excerpt: "An expression that represents doing something. When you write a lambda, the compiler generates an anonymous function object. Overrides (...",
      categories: [],
      tags: ["c++","beginner","programming"],
      id: 17
    });
    
  
    idx.add({
      title: "C++ Multi Threading",
      excerpt: "Multi threading &amp; Multiprocessing Multithreading is an ability of a platform(Operating System, Virtual Machine etc.) or application to create a...",
      categories: [],
      tags: ["c++","beginner","programming"],
      id: 18
    });
    
  
    idx.add({
      title: "Writing Jekyll Posts",
      excerpt: "HTML Elements Below is just about everything you’ll need to style in the theme. Check the source code to see...",
      categories: [],
      tags: ["intro","beginner","jekyll"],
      id: 19
    });
    
  
    idx.add({
      title: "Consistency beats commitment",
      excerpt: "“Consistency beats commitment every time” Today just finished “Building and Managing Your Career Plan” course on Pluralsight by Jason Alba....",
      categories: [],
      tags: ["intro","beginner","programming"],
      id: 20
    });
    
  
    idx.add({
      title: "High view recap of SOLID",
      excerpt: "This is little remainder about SOLID SOLID design principles Single Responsibility Principle (SRP) Class should only have a single responsibility...",
      categories: [],
      tags: ["intro","beginner","programming"],
      id: 21
    });
    
  
    idx.add({
      title: "Don't put random code just to make it work",
      excerpt: "Some of time consuming mistakes I made when learning some new language or technology is trying to make something to...",
      categories: [],
      tags: ["intro","beginner","programming"],
      id: 22
    });
    
  
    idx.add({
      title: "Effort multiplied by tools",
      excerpt: "I like Seth’s Blog . Once a day he put some witty two three liner that make me think. Today...",
      categories: [],
      tags: ["general","programming"],
      id: 23
    });
    
  
    idx.add({
      title: "Line ending drama",
      excerpt: "The Characters End of line characters are both control characters, so they should be invisible and only keep track. Carriage...",
      categories: [],
      tags: ["general","study","programming"],
      id: 24
    });
    
  
    idx.add({
      title: "Winning the lottery",
      excerpt: "The Source Seth’s Blog post The lottery winners(a secret of unhappiness) as always hit the bulls eye. You’re going to...",
      categories: [],
      tags: ["general","study","programming"],
      id: 25
    });
    
  
    idx.add({
      title: "Correct file permission using local server",
      excerpt: "Solution 1 Add yourself to the www-data group and set the setgid bit on the /var/www directory such that all...",
      categories: [],
      tags: ["web-development"],
      id: 26
    });
    
  
    idx.add({
      title: "C++ Pointers & Reference",
      excerpt: "Pointer is a variable that holds the address of another variable Pointer can be used to get to the original...",
      categories: [],
      tags: ["c++","study","programming"],
      id: 27
    });
    
  
    idx.add({
      title: "Shell suspend, kill and bring back on jobs",
      excerpt: "This should be true of any shell with job control Hitting Ctrl + z will suspend currently running process and...",
      categories: [],
      tags: ["linux","beginner","terminal"],
      id: 28
    });
    
  
    idx.add({
      title: "Spacemacs install on Windows",
      excerpt: "Spacemacs Recently I start using Spacemacs as Windows editor comforatable substitute for Neovim in Windows OS. As always installation is...",
      categories: [],
      tags: ["general","config","Windows","emacs"],
      id: 29
    });
    
  
    idx.add({
      title: "JavaScript defaults parameters",
      excerpt: "Function Defaults Perhaps one of most “easy” way to broke something is to use function defaults workarounds This is first...",
      categories: [],
      tags: ["programming","beginning","ES5","ES6"],
      id: 30
    });
    
  
    idx.add({
      title: "JavaScript For loops",
      excerpt: "Iteration in JavaScript JavaScript have few ways to loop over any type of data that is iterable. This allows to...",
      categories: [],
      tags: ["programming","JavaScript","begginer"],
      id: 31
    });
    
  
    idx.add({
      title: "JavaScript Programming Paradigms",
      excerpt: "JavaScirpt is one of the most important programming languages of all time, not simply because of its popularity, but because...",
      categories: [],
      tags: ["programming","JavaScript","begginer"],
      id: 32
    });
    
  
    idx.add({
      title: "JavaScript functional and lexical scope",
      excerpt: "To me, Scoping is the ruleset used to lookup variable values. Especially the ones that are not declared within the...",
      categories: [],
      tags: ["programming","beginning","javascript"],
      id: 33
    });
    
  
    idx.add({
      title: "JavaScript Software Composition",
      excerpt: "Function composition is the process of applying a function to the output of another function. Composing two functions is chaining...",
      categories: [],
      tags: ["programming","JavaScript","begginer"],
      id: 34
    });
    
  
    idx.add({
      title: "2018 Study Materials",
      excerpt: "After looking at lal the trends ahead, here’s one recomended learning plan for 2018 to remain competitive as a remote...",
      categories: [],
      tags: ["programming","JavaScript","begginer"],
      id: 35
    });
    
  


console.log( jQuery.type(idx) );

var store = [
  
    
    
    
      
      {
        "title": "Use Drush to Import/Export MySQL database file",
        "url": "http://nexusstar.name/hackpads/drush_export_dump_import_db/",
        "excerpt": "Export Database to file Drupal 6 &amp; 7 drush cc drush sql-dump &gt; ~/my-sql-dump-file-name.sql Drupal 8 drush cr drush sql-dump...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Favor object composition over class inheritance",
        "url": "http://nexusstar.name/hackpads/favor-object-composition-over-class-inheritance/",
        "excerpt": "“Design Patterns: Elements of Reusable Object-Oriented Software” Composition over inheritance (or composite reuse principle) in object-oriented programming (OOP) is the...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "File line ending in Windows of git repositories without normalization",
        "url": "http://nexusstar.name/hackpads/file-line-ending-in-windows-of-git/",
        "excerpt": "The proper way to get LF endings in Windows is to first set core.autocrlf to false: git config --global core.autocrlf...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Git made submodule from subfolder",
        "url": "http://nexusstar.name/hackpads/git-made-submodule-from-subfolder/",
        "excerpt": "At the beginning, we have something like this: &lt;git repository A&gt; someFolders someFiles someLib &lt;-- we want this to be...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Install / Update Ruby on windows",
        "url": "http://nexusstar.name/hackpads/install-update-ruby-windows/",
        "excerpt": "Install using downloads from RubyInstaller Check if correct version is installed with ruby --version if not correct version reported check...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Install AUR package-query and yaourt",
        "url": "http://nexusstar.name/hackpads/install-yaourt-packages/",
        "excerpt": "You can use the ~/temp/AUR/ to manually install AUR packages: $ mkdir -p ~/temp/AUR/ &amp;&amp; cd ~/temp/AUR/ First, we install...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "JavaScript as functional programming language",
        "url": "http://nexusstar.name/hackpads/javascript-and-functional-programming/",
        "excerpt": "JavaScript has the most important features needed for functional proragmming: First class functions: The ability to use functions as data...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Software composition notes",
        "url": "http://nexusstar.name/hackpads/lambda-calculus-software-comosition/",
        "excerpt": "In mathematics and computer science curring is the technique of translating the evaluation of a function that takes multiple arguments...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Responsive design",
        "url": "http://nexusstar.name/hackpads/responsive-design/",
        "excerpt": "Frontend Masters course I am currently watching from Jen Kramer bring to memory the article from Ethan Marcote definning what...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Using Iedit mode in Spacemacs",
        "url": "http://nexusstar.name/hackpads/spacemacs-iedit-mode-cheat-sheat/",
        "excerpt": "Spacemacs defines two states iedit state iedit-insert state Iedit can be started from search with SPC s e Key Binding...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Spacemacs Cheat Sheet",
        "url": "http://nexusstar.name/hackpads/spacemacs-vim-mode-cheatsheat/",
        "excerpt": "Get help, quit and configure Editor Basic commands Key Binding Description SPC q q Quit SPC z x Increase/decrease font...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Shell script's' idiom 2>&1 recap",
        "url": "http://nexusstar.name/hackpads/stdout-stderr-output-recap/",
        "excerpt": "There is excelent article from Brian Storti back from 2015 about shell script’s idiom 2&gt;&amp;1 So to note important points...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "SVN changelist command",
        "url": "http://nexusstar.name/hackpads/svn-changelist-your-best-friend/",
        "excerpt": "I’m not a big fan of SVN but it was inescapable evil in some projects. One of the best command...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Replace ^M windows line endings in vim",
        "url": "http://nexusstar.name/hackpads/vim-change-windows-line-breaks/",
        "excerpt": "When files comes from windows sometimes there are ^M at end of all lines which marks line ending. So to...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Work with numbers in Vim",
        "url": "http://nexusstar.name/hackpads/vim-work-with-numbers-in-insert-mode/",
        "excerpt": "The expression register = &lt;C-a&gt; &lt;C-x&gt;in normal mode Expression register = The expression register can evaluate a piece of Vim...",
        "teaser":
          
            null
          
      },
    
  
    
    
    
      
      {
        "title": "False confidence due to new found pride",
        "url": "http://nexusstar.name/false-confidence/",
        "excerpt": "Why do so many beginner programmers think they should build everything from scratch? Just read answers to this question on...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Rebuild with Jekyll",
        "url": "http://nexusstar.name/move-to-jekyll/",
        "excerpt": "Why My personal website went to too many changes. I start with simple static html and JS and later went...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "C++ Lambda",
        "url": "http://nexusstar.name/cpp_lambda/",
        "excerpt": "An expression that represents doing something. When you write a lambda, the compiler generates an anonymous function object. Overrides (...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "C++ Multi Threading",
        "url": "http://nexusstar.name/cpp_multithreading/",
        "excerpt": "Multi threading &amp; Multiprocessing Multithreading is an ability of a platform(Operating System, Virtual Machine etc.) or application to create a...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Writing Jekyll Posts",
        "url": "http://nexusstar.name/writing-jekyll-posts/",
        "excerpt": "HTML Elements Below is just about everything you’ll need to style in the theme. Check the source code to see...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Consistency beats commitment",
        "url": "http://nexusstar.name/consistency/",
        "excerpt": "“Consistency beats commitment every time” Today just finished “Building and Managing Your Career Plan” course on Pluralsight by Jason Alba....",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "High view recap of SOLID",
        "url": "http://nexusstar.name/solid-recap/",
        "excerpt": "This is little remainder about SOLID SOLID design principles Single Responsibility Principle (SRP) Class should only have a single responsibility...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Don't put random code just to make it work",
        "url": "http://nexusstar.name/dont-stutter/",
        "excerpt": "Some of time consuming mistakes I made when learning some new language or technology is trying to make something to...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Effort multiplied by tools",
        "url": "http://nexusstar.name/tools-of-the-trade/",
        "excerpt": "I like Seth’s Blog . Once a day he put some witty two three liner that make me think. Today...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Line ending drama",
        "url": "http://nexusstar.name/line-ending-drama/",
        "excerpt": "The Characters End of line characters are both control characters, so they should be invisible and only keep track. Carriage...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Winning the lottery",
        "url": "http://nexusstar.name/lottery-winner/",
        "excerpt": "The Source Seth’s Blog post The lottery winners(a secret of unhappiness) as always hit the bulls eye. You’re going to...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Correct file permission using local server",
        "url": "http://nexusstar.name/file-permissions/",
        "excerpt": "Solution 1 Add yourself to the www-data group and set the setgid bit on the /var/www directory such that all...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "C++ Pointers & Reference",
        "url": "http://nexusstar.name/cpp-pointers-and-reference/",
        "excerpt": "Pointer is a variable that holds the address of another variable Pointer can be used to get to the original...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Shell suspend, kill and bring back on jobs",
        "url": "http://nexusstar.name/shell-suspend-and-bring-back/",
        "excerpt": "This should be true of any shell with job control Hitting Ctrl + z will suspend currently running process and...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "Spacemacs install on Windows",
        "url": "http://nexusstar.name/spacemacs-windows/",
        "excerpt": "Spacemacs Recently I start using Spacemacs as Windows editor comforatable substitute for Neovim in Windows OS. As always installation is...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "JavaScript defaults parameters",
        "url": "http://nexusstar.name/javascript-defaults/",
        "excerpt": "Function Defaults Perhaps one of most “easy” way to broke something is to use function defaults workarounds This is first...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "JavaScript For loops",
        "url": "http://nexusstar.name/javascript-iterable-protocol/",
        "excerpt": "Iteration in JavaScript JavaScript have few ways to loop over any type of data that is iterable. This allows to...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "JavaScript Programming Paradigms",
        "url": "http://nexusstar.name/JavaScript-programming-paradigms/",
        "excerpt": "JavaScirpt is one of the most important programming languages of all time, not simply because of its popularity, but because...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "JavaScript functional and lexical scope",
        "url": "http://nexusstar.name/javascript-scope/",
        "excerpt": "To me, Scoping is the ruleset used to lookup variable values. Especially the ones that are not declared within the...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "JavaScript Software Composition",
        "url": "http://nexusstar.name/JavaScript-composition/",
        "excerpt": "Function composition is the process of applying a function to the output of another function. Composing two functions is chaining...",
        "teaser":
          
            null
          
      },
    
      
      {
        "title": "2018 Study Materials",
        "url": "http://nexusstar.name/2018-Study-Material/",
        "excerpt": "After looking at lal the trends ahead, here’s one recomended learning plan for 2018 to remain competitive as a remote...",
        "teaser":
          
            null
          
      }
    
  ]

$(document).ready(function() {
  $('input#search').on('keyup', function () {
    var resultdiv = $('#results');
    var query = $(this).val();
    var result = idx.search(query);
    resultdiv.empty();
    resultdiv.prepend('<p class="results__found">'+result.length+' Result(s) found</p>');
    for (var item in result) {
      var ref = result[item].ref;
      if(store[ref].teaser){
        var searchitem =
          '<div class="list__item">'+
            '<article class="archive__item" itemscope itemtype="http://schema.org/CreativeWork">'+
              '<h2 class="archive__item-title" itemprop="headline">'+
                '<a href="'+store[ref].url+'" rel="permalink">'+store[ref].title+'</a>'+
              '</h2>'+
              '<div class="archive__item-teaser">'+
                '<img src="'+store[ref].teaser+'" alt="">'+
              '</div>'+
              '<p class="archive__item-excerpt" itemprop="description">'+store[ref].excerpt+'</p>'+
            '</article>'+
          '</div>';
      }
      else{
    	  var searchitem =
          '<div class="list__item">'+
            '<article class="archive__item" itemscope itemtype="http://schema.org/CreativeWork">'+
              '<h2 class="archive__item-title" itemprop="headline">'+
                '<a href="'+store[ref].url+'" rel="permalink">'+store[ref].title+'</a>'+
              '</h2>'+
              '<p class="archive__item-excerpt" itemprop="description">'+store[ref].excerpt+'</p>'+
            '</article>'+
          '</div>';
      }
      resultdiv.append(searchitem);
    }
  });
});
